import '../CSS/App.css';
import "bootstrap/dist/css/bootstrap.min.css"
import { Navegacion } from '../layouts/Navegacion';
import { BrowserRouter, Route, Routes } from 'react-router-dom';
import {ProductoCreados} from "../pages/ProductoCreados"
import {ProductoDetalle} from "../pages/ProductoDetalle"
import {Signup} from "../pages/Signup"
import {Signin} from "../pages/Signin"
import {CrearProducto} from"../pages/CrearProducto"
function App() {
  return (
    <BrowserRouter>
    <Navegacion/>
    <Routes>
      <Route path='/' element={<ProductoCreados/>} />
      <Route path='/producto/:id' element={<ProductoDetalle/>} />
      <Route path='/signup' element={<Signup/>} />
      <Route path='/signin' element={<Signin/>} />
      <Route path='/crearproducto' element={<CrearProducto/>} />
    </Routes>
    </BrowserRouter>
  );
}

export default App;
