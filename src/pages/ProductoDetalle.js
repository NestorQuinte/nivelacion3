import axios from "axios"
import { useEffect, useState } from "react"
import { Card, Container, Row } from "react-bootstrap"
import { PRODUCTODETALLE_GET_ENDPOINT } from "../connections/endpoints"
import { useNavigate, useParams } from 'react-router-dom';
import { EliminarProductoBoton } from "../components/EliminarProductoBoton";

const ProductoDetalle= () => {
  const [producto, setProducto]=useState(null)
  const {id}= useParams()
  const navegar= useNavigate()

  useEffect(()=>{ 
    axios.get(`${PRODUCTODETALLE_GET_ENDPOINT}/${id}`)
    .then(respuesta=>{
      setProducto(respuesta.data)
    })
    .catch(err=>{
        console.error(err)
        navegar(-1)
    })
   }, [id, navegar])
    return (
      <Container className="mt-3 mtb-3">
        <Row className="justify-content-md-center">
          <col sm="12" md="8" lg="6">
            <h3 className="text-center">Producto Detalle</h3>
            <div className="row justify-content-center text-center">
              {producto &&(
                <Card style={{ width: '18rem' }}>
                  <Card.Img variant="top" src={producto.imagen} />
                  <Card.Body>
                    <Card.Title>{producto.titulo}</Card.Title>
                    <Card.Text>${producto.precio}</Card.Text>
                    <Card.Text>${producto.categoria}</Card.Text>
                    <Card.Text>${producto.descripcion}</Card.Text>
                    <EliminarProductoBoton id={producto.idProducto} titulo={producto.titulo}/>
                  </Card.Body>
                </Card>
              )}  
            </div>
            </col>
        </Row>
    
      </Container>
    );
  }
  
  export {ProductoDetalle};